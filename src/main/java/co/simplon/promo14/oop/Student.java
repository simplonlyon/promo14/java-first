package co.simplon.promo14.oop;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Student implements IFirst{
    private String name;
    private String firstName;
    private LocalDate birthDate;
    private int energy = 25;

    public Student(String name, String firstName, String birthDate) {
        this.name = name;
        this.firstName = firstName;
        this.birthDate = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    
    public Student(String name, String firstName, LocalDate birthDate) {
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }

    public Student(){
        
    }

    public String greeting() {
        return "Hello it's me "+name+" "+firstName;
    }

    public void learn(String subject) {
        if(energy > 5) {
            System.out.println("Learning "+subject);
            energy -= 5;
        } else {
            System.out.println("jpp");
        }
    }

    // public String getFullName() {
    //     return name+" "+firstName;
    // }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public LocalDate getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Student [birthDate=" + birthDate + ", energy=" + energy + ", firstName=" + firstName + ", name=" + name
                + "]";
    }

    @Override
    public String example(int arg) {
        
        return name.repeat(arg);
    }
    
}
