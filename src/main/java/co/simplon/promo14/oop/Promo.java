package co.simplon.promo14.oop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Promo implements IFirst{
    private String name;
    private List<Student> students = new ArrayList<>(List.of(
        new Student("Student 1", "First Name 1", "24/01/1999"),
        new Student("Student 2", "First Name 2", "10/09/1990")
    ));
    public Promo(String name) {
        this.name = name;
    }

    public void teach() {
        for (Student student : students) {
            student.learn("Java");
        }
    }

    public void loadStudents() {
        File file = new File("promo14.txt");
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                
                String line = scanner.nextLine();
                String[] values = line.split(",");
                Student student = new Student(values[1], values[0], values[2]);
                students.add(student);
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public String example(int arg) {
        
        return "Coucou"+arg;
    }

    


}
