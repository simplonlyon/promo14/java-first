package co.simplon.promo14;

import java.util.ArrayList;
import java.util.List;

import co.simplon.promo14.oop.IFirst;
import co.simplon.promo14.oop.Promo;
import co.simplon.promo14.oop.Student;
import co.simplon.promo14.rpg.Character;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // Variables instance = new Variables();

        // instance.firstCondition();

        // System.out.println(instance.ageFeedback(20));

        // instance.arrayXample();

        // Loop loop = new Loop();
        // // loop.firstLoop(20, "Tout à fait");
        // loop.multiTable();

        // Student student = new Student("name", "firstName", "25/01/2022");
        // System.out.println(student.getBirthDate());
        // System.out.println(student.greeting());
        // student.learn("Stuff");

        // Promo promo = new Promo("Promo 14");
        // promo.loadStudents();

        // IFirst first = new Promo("Promo first");
        

        // List<IFirst> liste = new ArrayList<>();
        // liste.add(promo);
        // liste.add(new Student("test", "test", "10/10/2020"));

        // for (IFirst iFirst : liste) {
        //     iFirst.example(10);
        // }

        Character character = new Character("Bobby");
        Character character2 = new Character("Dark Bobby");
        character.useItemOnSelf(2); //on équipe la Sword
        character.useItemOnTarget(2, character2);

        character2.useItemOnSelf(0);
    }
}