package co.simplon.promo14.base;

import java.util.ArrayList;
import java.util.List;

public class Variables {
    
    public void firstCondition() {

        byte varByte = -127;
        short varShort = 32767;
        int varInt = 220000000;
        Integer complexInt = null;
        long varLong = 200000000000000000l;

        float varFloat = 2.3f;
        double varDouble = 2.0000000000000000000004;

        boolean varBool = true;

        char varChar = 'a';


        String varString = "Une chaîne de caractères";

        varString = "je change la valeur";

        
        //Créer une variable name qui contiendra un prénom
        //Faire un if qui si le prénom contient bien votre prénom alors ça fera un
        //sysout de "It's me" sinon ça fera un sysout de "hello you"

        String name = "Jean";
        if(name == "Jean") {
            System.out.println("It's me");
        } else {
            System.out.println("hello you");
        }
        
    }


    public String ageFeedback(int age) {

       if(age < 18) {
           return "You're a minor";
       }
       if(age >= 1 && age <= 22) {
           return "You're a milennial";
       }
       if(age >= 18 && age <= 30) {
           return "you're age is young";
       }
       if(age > 30) {
           return "looking good !";
       }
       
       return "incorrect age";
    }

    public void arrayXample() {


        int[] monTableau = new int[10];

        monTableau[0] = 10;

        // monTableau[11] = 11; //ça pas possible pasque tableau trop petit
        System.out.println(monTableau[0]);

        List<String> maListe = new ArrayList<>();
        maListe.add("Coucou");
        maListe.set(0, "nouvelle valeur");
        System.out.println(maListe.get(0));
        
    }
}
