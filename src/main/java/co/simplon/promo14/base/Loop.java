package co.simplon.promo14.base;

public class Loop {
    
    public void firstLoop(int length, String message) {
        for(int i = 0; i < length; i++) {
            if(i % 2 == 0) {
                System.out.println(message.toUpperCase());

            } else {
                System.out.println(message.toLowerCase());

            }
        }
    }

    public void multiTable() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.printf("%4d", i*j );
            }
            System.out.println();
        }
    }
}
