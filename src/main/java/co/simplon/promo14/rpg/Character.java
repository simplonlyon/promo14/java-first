package co.simplon.promo14.rpg;

import java.util.ArrayList;
import java.util.List;

public class Character {
    protected int hp = 100;
    protected int mp = 100;
    private String name;
    private List<IUsable> inventory = new ArrayList<>(List.of(
        new Potion(10, 0),
        new Potion(0, 10),
        new Sword(20)
    ));

    public void useItemOnSelf(int index) {
        inventory.get(index).useOn(this);
    }

    public void useItemOnTarget(int index, Character target) {
        inventory.get(index).useOn(target);
    }

    public Character(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
