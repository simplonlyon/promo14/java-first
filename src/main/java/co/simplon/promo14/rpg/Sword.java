package co.simplon.promo14.rpg;

public class Sword implements IUsable {
    private int damage;
    private boolean equiped = false;


    public Sword(int damage) {
        this.damage = damage;
    }


    public void attack(Character character) {
        character.hp -= damage;
        System.out.println(character.getName()+" was hit and lost "+damage+" hp");
    }

    public void equip() {
        equiped = true;
        System.out.println("The sword was equiped");
    }


    @Override
    public void useOn(Character character) {
        if(!equiped) {
            equip();
        } else {
            attack(character);
        }
        
    }
    
}
 