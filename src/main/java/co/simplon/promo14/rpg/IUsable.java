package co.simplon.promo14.rpg;

public interface IUsable {
    void useOn(Character character);
}
