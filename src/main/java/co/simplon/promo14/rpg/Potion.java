package co.simplon.promo14.rpg;

public class Potion implements IUsable {
    private int hp;
    private int mp;
    public Potion(int hp, int mp) {
        this.hp = hp;
        this.mp = mp;
    }
    
    @Override
    public void useOn(Character character) {
        character.hp += hp;
        character.mp += mp;
        System.out.println(character.getName()+" drank a potion");
        
    }



    
    
}
