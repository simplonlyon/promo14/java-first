# First Java Project
Premier projet java fait avec la promo 14 en utilisant maven et Java 17

## Exercices Variables/Conditions
1. Créer une classe base.Variables et l'instancier dans le main
2. Dans la classe Variables, créer une méthode ageFeedback qui va renvoyer une chaîne de caractère et qui attendra un int age en argument
3. Dans cette méthode faire un if qui va vérifier si l'age est en dessous de 18, et si oui mettre un message vis à vis du fait qu'on est pas majeur
4. Faire un autre if qui si on est entre 1 et 22, nous dit qu'on est un millenial.Faire un autre if qui, si on est entre 18 et 30, nous dit quelque chose d'autre. Et enfin un qui dira quelque chose pour si c'est au dessus de 30
5. Appeler cette méthode depuis le main avec différents arguments, et faire un sysout de son retour

## Exercice Loop
1. Créer une classe base.Loop avec une méthode firstLoop() à l'intérieur
2. Dans cette méthode faire une boucle for classique qui va pour le moment faire 10 tour et qui à chaque tour va faire un sysout d'un message random, peu importe
3. Rajouter un argument int length qui indiquera le nombre de tour que la boucle doit faire, et l'utiliser pour que ça fasse le nombre de tour indiqué. Rajouter un autre argument en String qui sera le message à afficher
4. Faire une condition dans la boucle pour faire qu'on affiche le message en majuscule les tours pairs et en minuscule les tours impairs 
5. Faire une méthode multiTable qui va faire des boucles imbriquées pour afficher les tables de multiplication de 1 à 10 avec un affichage sous forme de tableau

## Exercice Classe Student
1. Créer une classe oop.Student qui aura comme propriétés, toutes private, name en String, firstName String et birthdate en LocalDate
2. Générer les getter/setter, un constructeur vide et un constructeur plein
3. Créer également un constructeur qui pour la birthdate attendra une chaîne de caractères plutôt qu'un LocalDate et qui du coup dans le constructeur fera une conversion de la String vers la LocalDate (je vous laisse chercher ça sur l'internet)
4. Rajouter une méthode greeting, qui fera un return de "Hello, it's me " suivi du nom/prénom du Student
5. Rajouter une propriété, private toujours, energy initialisée à 25 puis une méthode learn(String subject) qui fera un sysout de "I'm learning" suivi du subject et baissera l'energy si elle n'est pas à zéro, et sinon qui fera un sysout de "jpp" 